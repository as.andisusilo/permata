import React from 'react';
import {Svg, Path} from 'react-native-svg';
const Apple = () => (
  <Svg
    width="24"
    height="24"
    viewBox="0 0 842 999"
    fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <Path
      d="M702 959C647.8 1011.6 588 1003.4 531 978.6C470.4 953.3 415 951.7 351 978.6C271.3 1013 229 1003 181 959C-90 680 -50 255 258 239C332.7 243 385 280.3 429 283.4C494.4 270.1 557 232 627 237C711.1 243.8 774 277 816 336.7C643 440.7 684 668.7 842.9 732.7C811.1 816.2 770.3 898.7 701.9 959.7L702 959ZM423 236C414.9 112 515.4 10 631 0C646.9 143 501 250 423 236Z"
      fill="black"
    />
  </Svg>
);

export default Apple;
