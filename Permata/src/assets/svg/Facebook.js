import React from 'react';
import {Svg, Path} from 'react-native-svg';

const Facebook = () => (
  <Svg
    width="12"
    height="24"
    viewBox="0 0 12 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <Path
      d="M2.97779 23.8555V12.594H1.52588e-05V8.53929H2.97779V5.07607C2.97779 2.35464 4.73678 -0.144531 8.78985 -0.144531C10.4309 -0.144531 11.6443 0.0127887 11.6443 0.0127887L11.5487 3.79917C11.5487 3.79917 10.3112 3.78712 8.96073 3.78712C7.49913 3.78712 7.26496 4.46068 7.26496 5.57863V8.53929H11.6649L11.4735 12.594H7.26496V23.8555H2.97779Z"
      fill="#0984E3"
    />
  </Svg>
);

export default Facebook;
