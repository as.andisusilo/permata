import {View, Text, TouchableOpacity, Image, Button,StatusBar} from 'react-native';
import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import styles from './style';
import IMAGE from '../../assets/img';

import { useTranslation } from 'react-i18next';

const First = props => {
  const [forButton, setforButton] = useState('login');
  const { t, i18n } = useTranslation();

  useEffect(() => {}, []);

  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor="white"
        barStyle={'dark-content'}
        />
          <TouchableOpacity
            style={styles.EN}
            onPress={() => props.navigation.navigate('SelectBahasa')}>
            <Text style={styles.textEN}>{t('common:bahasa')}</Text>
          </TouchableOpacity>
      <View style={styles.containerImage}>
        <View>
        </View>
        <Image
          source={IMAGE.loginImage}
          resizeMode="contain"
          style={styles.SplashImage}
        />
      </View>
      <View style={styles.containerTitle}>
        <Text style={styles.title}>
        {t('common:TitleFirst')}
        </Text>
        <Text
          style={{
            ...styles.title,
            fontSize: 12,
            marginTop: 10,
            fontWeight: 'normal',
          }}>
          {t('common:TitleFirstChild')}
        </Text>
      </View>
      <View style={styles.containerButton}>
        <TouchableOpacity
          style={styles.button}
          >
          <Text style={styles.textButton}>{t('common:Signup')}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
            props.navigation.navigate('Login');
          }}
          style={{...styles.button, backgroundColor: '#1571dd'}}>
          <Text style={{...styles.textButton, color: 'white'}}>{t('common:login')}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default First;
