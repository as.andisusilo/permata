import {StyleSheet, Dimensions, Platform} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerImage: {
    backgroundColor: '#f2f2f2',
    borderRadius: 40,
    height: Platform.OS == 'android' ? windowHeight * 0.55 :windowHeight * 0.50,
    width: windowWidth * 0.9,
    marginTop: Platform.OS == 'ios' ? 60 : 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  SplashImage: {
    height: windowHeight * 0.7,
    width: windowWidth * 0.9,
  },
  containerTitle: {
    height: windowHeight * 0.2,
    width: windowWidth * 1,
    marginTop: -20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 32,
    fontWeight: '600',
    color: 'black',
    textAlign: 'center',
  },
  containerButton: {
    height: windowHeight * 0.25,
    width: windowWidth * 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  button: {
    flex: 0.5,
    marginTop:Platform.OS =='android' ? -40 :-80,
    marginLeft: 20,
    borderRadius: 8,
    borderColor: 'grey',
    borderWidth:0.5,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  textButton: {
    fontSize: 12,
  },
  EN:{
    position:'absolute',
    top:Platform.OS =='android' ? 30 : 70,
    right:40,
    backgroundColor:'#1571dd',
    borderRadius:100,
    zIndex:100000,
    height:windowHeight*0.04,
    width:windowHeight*0.04,
    justifyContent:'center',
    alignItems:'center'

  },
  textEN:{
    color:'white',
    fontWeight:'bold'
  }
});

export default styles;
