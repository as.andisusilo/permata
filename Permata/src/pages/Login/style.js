import {StyleSheet, Dimensions, Platform} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
    marginTop:Platform.OS =='android' ? -20 :0
  },
  containerHeader: {
    height: windowHeight * 0.1,
    width: windowWidth * 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginTop: 20,
  },
  containerEn: {
    height: windowHeight * 0.04,
    width: windowHeight * 0.04,
    backgroundColor: '#1571dd',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textEN: {
    color: 'white',
    fontSize: 12,
  },
  containerTitle: {
    height: windowHeight * 0.1,
    width: windowWidth * 1,
    // alignItems:'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  textSub1: {
    fontSize: 24,
    color: 'black',
    fontWeight: 'bold',
  },
  textSub2: {
    marginTop: 5,
    fontSize: 12,
    color: 'black',
    fontWeight: 'normal',
  },
  containerTextinput: {
    height: windowHeight * 0.3,
    width: windowWidth * 1,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    // alignItems:'center'
  },
  textInput: {
    height: windowHeight * 0.05,
    width: windowWidth * 0.9,
    backgroundColor: '#f6f6f6',
    borderRadius: 10,
    paddingHorizontal: 20,
    borderWidth:1,
    borderColor:'#1571dd'
  },
  containerTextIn: {
    flex: 0.5,
    justifyContent: 'center',
  },
  textUsername:{
    marginVertical:10,
    fontSize:14,
    color:'black'
  },
  containerButton:{
    paddingVertical:15,
    backgroundColor:'#1571dd',
    borderRadius:10,
    justifyContent:'center',
    alignItems:'center',
    marginHorizontal:20

  },
  rowSvg:{
    flexDirection:'row',
    justifyContent:'space-between',
    marginTop:20,
    marginBottom:50
  },
  containerFooter:{
    height:windowHeight*0.4,
    width:windowWidth*1,
    // justifyContent:'space-between',
    alignItems:'center',
  },
  containerIcon:{
    // backgroundColor:'grey',
    borderWidth:0.5,
    borderColor:'#e5e6e5',
    borderRadius:8,
    height:windowHeight*0.06,
    width:windowHeight*0.07,
    justifyContent:"center",
    alignItems:'center',
    marginHorizontal:15,
  },
  containerHide:{
    position:'absolute',
    top:10,
    right:20
  },
  textNot:{
    fontSize: 12,
    marginVertical: 20,
    fontWeight: 'normal',
    color: 'black',
  },
  textRegister:{
    fontSize: 12,
    marginVertical: 20,
    fontWeight: 'normal',
    color: 'blue',
  }
});

export default styles;
