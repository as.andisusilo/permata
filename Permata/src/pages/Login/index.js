import {View, Text, TouchableOpacity, TextInput,StatusBar} from 'react-native';
import React, {useEffect, useState} from 'react';
import Apple from '../../assets/svg/Apple';
import Facebook from '../../assets/svg/Facebook';
import Google from '../../assets/svg/Google';
import styles from './style';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux';
import { useTranslation } from 'react-i18next';

const Login = props => {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [hideUsername, setHideUsername] = useState(false);
  const [hidePassword, setHidePassword] = useState(false);
  const [secureText, setsecureText] = useState(true);
  const { t, i18n } = useTranslation();

  const dataLogin = {
    username: username?.toLowerCase(),
    // password: password?.toLowerCase(),
  };
  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor="white"
        barStyle={'dark-content'}
        />
      <View style={styles.containerHeader}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <FontAwesome5 size={20} color={'black'} name={'chevron-left'} />
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>props.navigation.navigate('SelectBahasa')} style={styles.containerEn}>
          <Text style={styles.textEN}>{t('common:bahasa')}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.containerTitle}>
        <Text style={styles.textSub1}>{t('common:Welcome')}</Text>
        <Text style={styles.textSub2}>
        {t('common:LoginTitle')}
        </Text>
      </View>
      <View style={styles.containerTextinput}>
        <View style={styles.containerTextIn}>
          <Text style={styles.textUsername}>{t('common:Username')}</Text>
          <View>
            <TextInput
              value={username}
              onChangeText={text => setUsername(text)}
              onFocus={() => setHideUsername(true)}
              onBlur={() => setHideUsername(false)}
              placeholder={t('common:PlaceholderUsername')}
              style={{
                ...styles.textInput,
                borderWidth: hideUsername ? 1 : 0,
                backgroundColor: hideUsername ? '#f6f6f6' : '#e5e6e5',
              }}></TextInput>
          </View>
        </View>
        <View style={styles.containerTextIn}>
          <Text style={styles.textUsername}>{t('common:Password')}</Text>
          <View>
            <TextInput
              secureTextEntry={secureText ? true : false}
              value={password}
              onChangeText={text => setPassword(text)}
              onFocus={() => setHidePassword(true)}
              onBlur={() => setHidePassword(false)}
              placeholder={t('common:PlaceholderPassword')}
              style={{
                ...styles.textInput,
                borderWidth: hidePassword ? 1 : 0,
                backgroundColor: hidePassword ? '#f6f6f6' : '#e5e6e5',
              }}></TextInput>
            <TouchableOpacity
              onPress={() => setsecureText(!secureText)}
              style={styles.containerHide}>
              <FontAwesome5
                size={20}
                color={'black'}
                name={secureText ? 'eye-slash' : 'eye'}
              />
            </TouchableOpacity>
          </View>
        </View>
        <Text style={{...styles.textUsername, color: '#1571dd'}}>
        {t('common:Forget')}
        </Text>
      </View>
      <TouchableOpacity
      disabled={props.isLoading ? true :false}
        onPress={() => props.sendLogin(dataLogin, props.navigation)}
        style={{
          ...styles.containerButton,
          backgroundColor: props.isLoading ? 'grey' : '#1571dd',
        }}>
        <View>
          <Text style={{color: 'white'}}>
            {props.isLoading ? t('common:Loading') : t('common:Login')}
          </Text>
        </View>
      </TouchableOpacity>
      <View style={styles.containerFooter}>
        <Text
          style={{
            ...styles.textSub1,
            fontSize: 12,
            marginTop: 90,
            fontWeight: 'normal',
          }}>
          {t('common:Continue')}
        </Text>
        <View style={styles.rowSvg}>
          <View style={styles.containerIcon}>
            <Google />
          </View>
          <View style={styles.containerIcon}>
            <Facebook />
          </View>
          <View style={styles.containerIcon}>
            <Apple />
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={{
              ...styles.textNot,
            }}>
            {t('common:NotAmember')}
          </Text>
          <Text
            style={{
              ...styles.textRegister,color :'#1571dd'
            }}>
            {t('common:Register')}
          </Text>
        </View>
      </View>
    </View>
  );
};

const reduxDispacth = dispatch => ({
  sendLogin: (a, n) => dispatch({type: 'LOGIN_START', passing: a, navigasi: n}),
});

const reduxState = state => ({
  isLoading: state.login.isLoading,
});
export default connect(reduxState, reduxDispacth)(Login);
