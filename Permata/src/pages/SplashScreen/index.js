import {View, Text, TouchableOpacity, Image, Dimensions} from 'react-native';
import React from 'react';
import IMAGE from '../../assets/img';
import style from './style';

const Splash = () => {
  return (
    <View style={style.container}>
      <Image
        resizeMode="contain"
        source={IMAGE.loginImage}
        style={style.SplashImage}
      />
    </View>
  );
};

export default Splash;
