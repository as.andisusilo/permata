import {StyleSheet, Dimensions,Platform} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center'
  },
  SplashImage:{
    height:windowHeight*0.7,
    width:windowWidth*0.9
  }
});

export default styles;
