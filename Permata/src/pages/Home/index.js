import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  FlatList,
  Platform,
  StatusBar
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Apple from '../../assets/svg/Apple';
import Facebook from '../../assets/svg/Facebook';
import Google from '../../assets/svg/Google';
import styles from './style';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Epay from '../../assets/svg/Epayslip';
import Bpjs from '../../assets/svg/Bpjs';
import Loan from '../../assets/svg/Loan';
import ImagePicker from 'react-native-image-crop-picker';
import IMAGE from '../../assets/img';
import { useTranslation } from 'react-i18next';

const Home = props => {
  const { t, i18n } = useTranslation();

  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [hideUsername, setHideUsername] = useState(false);
  const [hidePassword, setHidePassword] = useState(false);
  const [secureText, setsecureText] = useState(false);
  const [Photo, setPhoto] = useState();
  const dataLogin = {
    username: username?.toLowerCase(),
    password: password?.toLowerCase(),
  };

  const data = [
    {
      name: t('common:Applycant'),
      image: IMAGE.apllicant,
    },
    {
      name: t('common:Bookmark'),
      image: IMAGE.bookmark,
    },
    {
      name: t('common:JobPosting'),
      image: IMAGE.jobPosting,
    },
    {
      name: t('common:YourProject'),
      image: IMAGE.yourProject,
    },
  ];
  const pickPhoto = async () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      if(Platform.OS =='android') {
        setPhoto(image.path)
      }else{

        setPhoto(image.sourceURL);
      }
    });
  };

  useEffect(() => {

  }, [Photo])
  

  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor="white"
        barStyle={'dark-content'}
        />
      <View style={styles.absolute}>
        <Text style={styles.textabsolute}>5</Text>
      </View>
      <View style={styles.containerHeader}>
        <TouchableOpacity
          style={{marginRight: 20}}
          onPress={() => props.navigation.goBack()}>
          <FontAwesome5 size={20} color={'black'} name={'bell'} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => pickPhoto()}>
          <Image source={{uri: Photo}} style={styles.containerEn}></Image>
        </TouchableOpacity>
      </View>
      <View style={styles.containerTextinput1}>
        <View style={styles.containerTextIn}>
          <View>
            <TextInput
              value={username}
              onChangeText={text => setUsername(text)}
              placeholder="Search your job"
              style={{
                ...styles.textInput,
                borderWidth: 1,
                backgroundColor: 'white',
                borderColor: '#e6e6e6',
              }}></TextInput>
            <TouchableOpacity
              onPress={() => setsecureText(!secureText)}
              style={styles.containerHide}>
              <FontAwesome5 size={20} color={'black'} name={'search'} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={styles.containerTitle}>
        <Text style={styles.textSub1}>{t('common:WelcomeHome')}</Text>
        <Text style={styles.textSub2}>{t('common:DesHome')} </Text>
      </View>
      <View style={{...styles.containerTextinput1}}>
        <View style={styles.rowSvg}>
          <View style={styles.containerIconMerge}>
            <Epay />
            <Text style={styles.textInIcon}>ePay Slip</Text>
          </View>
          <View style={styles.containerIconMerge}>
            <Bpjs />
            <Text style={styles.textInIcon}>BPJS</Text>
          </View>
          <View style={styles.containerIconMerge}>
            <Loan />
            <Text style={styles.textInIcon}>Loan</Text>
          </View>
        </View>
      </View>
      <View style={styles.containerTitle}>
        <Text style={styles.textSub1}>{t('common:TitleMenuHome')}</Text>
        <Text style={styles.textSub2}>{t('common:desMenuHome')} </Text>
      </View>
      <View style={styles.containerMenu}>
        <FlatList
          contentContainerStyle={{justifyContent: 'space-between'}}
          numColumns={2}
          data={data}
          renderItem={({item}) => {
            return (
              <View style={styles.containerInF}>
                <View style={{flex: 0.4, marginLeft: 10}}>
                  <View style={styles.roundImage}>
                    <Image source={item.image} style={styles.image} />
                  </View>
                </View>
                <View style={{flex: 0.6}}>
                  <Text>{item.name}</Text>
                </View>
              </View>
            );
          }}
        />
      </View>
    </View>
  );
};

export default Home;
