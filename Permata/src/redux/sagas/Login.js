import {takeLatest, put} from 'redux-saga/effects';
import React from "react";
import { View, StyleSheet, ToastAndroid, Button, StatusBar,Platform,Alert } from "react-native";
import axios from 'axios';
import qs from 'qs';
import AsyncStorage from '@react-native-async-storage/async-storage';

const showToast = () => {
  ToastAndroid.show("Login Success !", ToastAndroid.SHORT);
};
const showToastFail = () => {
  ToastAndroid.show("Try Again !", ToastAndroid.SHORT);
};

function* LOGINHERO(action) {
  var data = qs.stringify({
    username: action.passing.username,
  });
  
  try {
    const LoginApi = yield axios({
      method: 'post',
      url: 'https://tasklogin.herokuapp.com/api/login',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: data,
    });
    if (LoginApi && LoginApi.data) {
      yield put({
        type: 'LOGIN_SUCCESS',
        data: LoginApi.data,
      });
      action.navigasi.navigate('Home')
      const saveData = async(value)=>{
        try {
          await AsyncStorage.setItem('@Token', value)
          if(Platform.OS == 'android'){

            showToast()
          }else{
            Alert.alert('Login Success')
          }

        } catch (e) {
          // saving error
          showToastFail()

        }
      }
      saveData(LoginApi.data.access_token)
    }
  } catch (err) {
    console.log(err);
    yield put({
      type: 'LOGIN_FAILED',
      error: err.response.data.status_message,
    });
    if(Platform.OS == 'android'){

      showToastFail()
    }else{
      Alert.alert('Try Again')
    }  }
}

function* LOGIN() {
  yield takeLatest('LOGIN_START', LOGINHERO);
}

export default LOGIN;
