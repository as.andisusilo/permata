import React, {useState, useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../pages/Home';
import First from '../pages/First';
import Login from '../pages/Login';
import Splash from '../pages/SplashScreen';
import {CardStyleInterpolators} from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SelectBahasa from '../components/LanguageSelector'
const Stack = createStackNavigator();

const AppStack = (props) => {
  const [load, setLoad] = useState(true);
  const [tokenApi, setToken] = useState()
  const time = () => {
    setTimeout(() => {
      setLoad(false);
    }, 2000);
  };
  useEffect(() => {
    const getToken = async () => {
      time();
      const token = await AsyncStorage.getItem('@Token');
      if(token){
        setToken(token)
      }
    };
    getToken();
  }, [tokenApi]);

  if (load) {
    return <Splash></Splash>;
  }

  return (
    <Stack.Navigator
    initialRouteName={tokenApi ? 'Home' :'First'}
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name="First"
        component={First}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
        <Stack.Screen
        name="SelectBahasa"
        component={SelectBahasa}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
    </Stack.Navigator>
  );
};

export default AppStack;
