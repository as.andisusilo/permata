import {View, Text} from 'react-native';
import React,{useEffect} from 'react';
import { Provider } from 'react-redux';
import { useStore } from 'react-redux';
import Loan from './src/assets/svg/Loan';
import { NavigationContainer } from '@react-navigation/native';
import storeRedux from './src/redux/store';
import AppStack from './src/route/Route';
import './src/constants/IMLocalize';

const App = () => {
  return (
    <Provider store={storeRedux}>
      <NavigationContainer>
        <AppStack/>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
